package com.github.json.deserializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.ContextualSerializer;
import com.github.json.annotations.JsonFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;


public class JsonFormatterSerializer extends JsonSerializer<Object> implements ContextualSerializer {
    private static final Logger log = LoggerFactory.getLogger(JsonFormatterSerializer.class);
    private String fullName;
    private String format;

    public JsonFormatterSerializer() {
    }

    private JsonFormatterSerializer(String fullName, String format) {
        this.fullName = fullName;
        this.format = format;
    }

    @Override
    public void serialize(Object value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        try {
            String writeValue = String.format(format, value);
            gen.writeString(writeValue);
        }catch (Exception e) {
            log.error(String.format("%s use %s value value %s error.", fullName, format, value), e);
        }
    }

    @Override
    public JsonSerializer<?> createContextual(final SerializerProvider serializerProvider,
                                              final BeanProperty beanProperty) throws JsonMappingException {

        if (beanProperty == null){
            return serializerProvider.findNullValueSerializer(null);
        }

        Class<?> clazz = beanProperty.getType().getRawClass();
        if (!(CharSequence.class.isAssignableFrom(clazz)
                || Number.class.isAssignableFrom(clazz))) {
            return serializerProvider.findValueSerializer(beanProperty.getType(), beanProperty);
        }

        JsonFormatter jsonFormatter = beanProperty.getAnnotation(JsonFormatter.class);
        if (jsonFormatter == null) {
            jsonFormatter = beanProperty.getContextAnnotation(JsonFormatter.class);
        }
        if (jsonFormatter == null) {
            return serializerProvider.findValueSerializer(beanProperty.getType(), beanProperty);
        }

        return new JsonFormatterSerializer(beanProperty.getFullName().toString(), jsonFormatter.value());
    }
}
