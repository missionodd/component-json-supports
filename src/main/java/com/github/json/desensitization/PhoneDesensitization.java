package com.github.json.desensitization;


import com.github.json.utils.MaskingUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 手机号脱敏器 默认只保留前3位和后4位
 */
public class PhoneDesensitization implements StringDesensitization {

    /**
     * 手机号正则
     */
    private static final Pattern DEFAULT_PATTERN = Pattern.compile("(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\\d{8}");

    /**
     * 手机号脱敏 只保留前3位和后4位
     */
    @Override
    public String desensitize(String target) {
        Matcher matcher = DEFAULT_PATTERN.matcher(target.trim());
        while (matcher.find()) {
            String group = matcher.group();
            String masking = MaskingUtils.dealMasking(group, 3, 7, '*');
            target = target.replace(group, masking);
        }
        return target;
    }
}
