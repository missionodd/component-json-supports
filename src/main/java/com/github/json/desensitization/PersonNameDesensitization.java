package com.github.json.desensitization;

import com.github.json.utils.MaskingUtils;

public class PersonNameDesensitization implements StringDesensitization {

    @Override
    public String desensitize(String target) {
        return  MaskingUtils.dealMasking(target, 1, Math.max(2, target.length()-1), '*');
    }
}
