package com.github.json.desensitization;


import com.github.json.utils.MaskingUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 邮箱脱敏器 默认只保留域名
 */
public class EmailDesensitization implements StringDesensitization {

    /**
     * 邮箱正则(半匹配)
     */
    private static final Pattern DEFAULT_PATTERN = Pattern.compile("([A-Za-z0-9_\\-.])+@([A-Za-z0-9_\\-.])+\\.([A-Za-z]{2,4})");

    /**
     * 邮箱脱敏
     */
    @Override
    public String desensitize(String target) {
        Matcher matcher = DEFAULT_PATTERN.matcher(target);
        while (matcher.find()) {
            String group = matcher.group();
            String masking = MaskingUtils.dealMasking(group, 0,  group.lastIndexOf("@") , '*');
            target = target.replace(group, masking);
        }
        return target;
    }


}
