package com.github.json.desensitization;

/**
 * 脱敏器接口
 */
public interface Desensitization<T> {


    /**
     * 脱敏实现
     *
     * @param target 脱敏对象
     * @return 脱敏返回结果
     */
    T desensitize(T target);

}
