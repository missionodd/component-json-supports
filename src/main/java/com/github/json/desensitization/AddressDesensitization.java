package com.github.json.desensitization;

import com.github.json.utils.MaskingUtils;

public class AddressDesensitization implements StringDesensitization {

    @Override
    public String desensitize(String target) {
        return  MaskingUtils.dealMasking(target, 6, target.length(), '*');
    }
}
