package com.github.json.desensitization;

import com.github.json.utils.MaskingUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BankCardDesensitization implements StringDesensitization {


    private static final Pattern DEFAULT_PATTERN = Pattern.compile("^([1-9]{1})(\\d{11}|\\d{15}|\\d{16}|\\d{17}|\\d{18})$");

    /**
     * 默认只保留后4位
     */
    @Override
    public String desensitize(String target) {
        Matcher matcher = DEFAULT_PATTERN.matcher(target.trim());
        while (matcher.find()) {
            String group = matcher.group();
            int last = group.length() - 4;
            String masking = MaskingUtils.dealMasking(group, 0, last, '*');
            target = target.replace(target,  masking);
        }
        return target;
    }
}
