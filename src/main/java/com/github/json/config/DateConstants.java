package com.github.json.config;

public class DateConstants {
    /**
     * 时间格式化常量
     */
    public static final String yyyy_MM_dd = "yyyy-MM-dd";
    public static final String yyyy_MM_dd_HH_mm_ss = "yyyy-MM-dd HH:mm:ss";
    public static final String HH_mm_ss = "HH:mm:ss";
    public static final String TIME_ZONE = "GMT+8";


}
