package com.github.json.annotations;

import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.github.json.desensitization.IDCardDesensitization;

import java.lang.annotation.*;

/**
 * 中华人民共和国身份证 脱敏注解
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@JacksonAnnotationsInside
@Desensitize(using = IDCardDesensitization.class)
@Documented
public @interface IDCardDesensitize {
}
