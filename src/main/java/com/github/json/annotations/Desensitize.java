package com.github.json.annotations;

import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.github.json.desensitization.Desensitization;
import com.github.json.deserializer.ObjectDesensitizeSerializer;
import java.lang.annotation.*;

/**
 * 对象脱敏 注解
 */
@Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@JacksonAnnotationsInside
@JsonSerialize(using = ObjectDesensitizeSerializer.class)
@Documented
public @interface Desensitize {
    /**
     * 对象脱敏器实现
     */
    Class<? extends Desensitization<?>> using();


}