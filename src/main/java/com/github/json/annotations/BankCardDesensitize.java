package com.github.json.annotations;

import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.github.json.desensitization.BankCardDesensitization;

import java.lang.annotation.*;
/**
 * 银行卡脱敏
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@JacksonAnnotationsInside
@Desensitize(using = BankCardDesensitization.class)
@Documented
public @interface BankCardDesensitize {
}
