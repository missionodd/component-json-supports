package com.github.json.annotations;

import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.github.json.deserializer.EpochDayDeserializer;
import com.github.json.serializer.EpochDaySerializer;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 组合注解
 * LocalDate 序列化 5位天级时间戳
 * 5位天级时间戳 反序列化 LocalDate
 */
@Retention(RetentionPolicy.RUNTIME)
@JacksonAnnotationsInside
@JsonSerialize(using = EpochDaySerializer.class)
@JsonDeserialize(using = EpochDayDeserializer.class)
public @interface JsonEpochDayTrans {
}
