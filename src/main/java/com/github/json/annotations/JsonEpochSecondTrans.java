package com.github.json.annotations;

import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.github.json.deserializer.EpochSecondDeserializer;
import com.github.json.serializer.EpochSecondSerializer;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 组合注解
 * LocalDateTime 序列化 10位秒级时间戳
 * 10位秒级时间戳 反序列化 LocalDateTime
 */
@Retention(RetentionPolicy.RUNTIME)
@JacksonAnnotationsInside
@JsonSerialize(using = EpochSecondSerializer.class)
@JsonDeserialize(using = EpochSecondDeserializer.class)
public @interface JsonEpochSecondTrans {
}
