package com.github.json.annotations;

import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.github.json.deserializer.EpochMilliDeserializer;
import com.github.json.serializer.EpochMilliSerializer;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 组合注解
 * LocalDateTime 序列化 13位毫秒级时间戳
 * 13位毫秒级时间戳 反序列化 LocalDateTime
 */
@Retention(RetentionPolicy.RUNTIME)
@JacksonAnnotationsInside
@JsonSerialize(using = EpochMilliSerializer.class)
@JsonDeserialize(using = EpochMilliDeserializer.class)
public @interface JsonEpochMilliTrans {
}
