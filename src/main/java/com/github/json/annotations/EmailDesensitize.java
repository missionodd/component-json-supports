package com.github.json.annotations;

import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.github.json.desensitization.EmailDesensitization;


import java.lang.annotation.*;

/**
 * 邮箱脱敏 注解
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@JacksonAnnotationsInside
@Desensitize(using = EmailDesensitization.class)
@Documented
public @interface EmailDesensitize {
}