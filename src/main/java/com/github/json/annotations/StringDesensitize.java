package com.github.json.annotations;

import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.github.json.desensitization.AddressDesensitization;
import com.github.json.desensitization.StringDesensitization;

import java.lang.annotation.*;

/**
 * 字符脱敏
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@JacksonAnnotationsInside
@Desensitize(using = StringDesensitization.class)
@Documented
public @interface StringDesensitize {
}
