package com.github.json.annotations;

import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.github.json.desensitization.AddressDesensitization;
import com.github.json.desensitization.PersonNameDesensitization;

import java.lang.annotation.*;

/**
 * 地址脱敏
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@JacksonAnnotationsInside
@Desensitize(using = PersonNameDesensitization.class)
@Documented
public @interface PersonNameDesensitize {
}
