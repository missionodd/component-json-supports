package com.github.json.annotations;

import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.github.json.desensitization.PhoneDesensitization;


import java.lang.annotation.*;

/**
 * 电话脱敏 注解
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@JacksonAnnotationsInside
@Desensitize(using = PhoneDesensitization.class)
@Documented
public @interface PhoneDesensitize {
}