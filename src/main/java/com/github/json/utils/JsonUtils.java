package com.github.json.utils;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer;
import com.github.json.config.DateConstants;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

/***
 * JSON辅助类
 */
public class JsonUtils {
	private static final Logger log = LoggerFactory.getLogger(JsonUtils.class);

	private static ObjectMapper objectMapper;

	/**
	 * 初始化ObjectMapper
	 */
	public static ObjectMapper getObjectMapper(){
		if(objectMapper != null){
			return objectMapper;
		}
		objectMapper = new ObjectMapper();

		// Include.Include.ALWAYS 默认
		// Include.NON_DEFAULT 属性为默认值不序列化
		// Include.NON_EMPTY 属性为 空（""） 或者为 NULL 都不序列化，则返回的json是没有这个字段的。这样对移动端会更省流量
		// Include.NON_NULL 属性为NULL 不序列化
		objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

		// 设置时区和日期转换
		objectMapper.setDateFormat(new SimpleDateFormat(DateConstants.yyyy_MM_dd_HH_mm_ss)); //推荐从 "spring.jackson.date-format" 获取
		objectMapper.setTimeZone(TimeZone.getDefault());  //推荐从 "spring.jackson.time-zone" 获取

		SimpleModule simpleModule = new SimpleModule();
		//java8+ Local时间格式化 序列化和反序列化
		simpleModule.addSerializer(LocalDateTime.class,new LocalDateTimeSerializer(DateTimeFormatter.ofPattern(DateConstants.yyyy_MM_dd_HH_mm_ss)));
		simpleModule.addSerializer(LocalDate.class,new LocalDateSerializer(DateTimeFormatter.ofPattern(DateConstants.yyyy_MM_dd)));
		simpleModule.addSerializer(LocalTime.class,new LocalTimeSerializer(DateTimeFormatter.ofPattern(DateConstants.HH_mm_ss)));
		simpleModule.addDeserializer(LocalDateTime.class,new LocalDateTimeDeserializer(DateTimeFormatter.ofPattern(DateConstants.yyyy_MM_dd_HH_mm_ss)));
		simpleModule.addDeserializer(LocalDate.class,new LocalDateDeserializer(DateTimeFormatter.ofPattern(DateConstants.yyyy_MM_dd)));
		simpleModule.addDeserializer(LocalTime.class,new LocalTimeDeserializer(DateTimeFormatter.ofPattern(DateConstants.HH_mm_ss)));
		objectMapper.registerModule(simpleModule);


		// 不存在的属性，直接忽略。解决报错：com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException: Unrecognized field
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return objectMapper;
	}

	/**
	 * 设置 objectMapper
	 * @param mapper
	 */
	public static void setObjectMapper(ObjectMapper mapper){
		objectMapper = mapper;
	}


		/**
         * 将Java对象转换为Json String
         *
         * @param object object
         * @return String
         */
	public static String stringify(Object object) {
		return toJSON(object);
	}


	/**
	 * 转换对象为JSON字符串
	 *
	 * @param model model
	 * @return String
	 */
	public static String toJSON(Object model) {
		try {
			return getObjectMapper().writeValueAsString(model);
		} catch (Exception e) {
			log.error("Java转Json异常", e);
			return null;
		}
	}

	/**
	 * 转换对象为JSON字符串
	 *
	 * @param model model
	 * @return String
	 */
	public static String toPrettyJSON(Object model) {
		try {
			return getObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(model);
		} catch (Exception e) {
			log.error("美化Json异常", e);
			return null;
		}
	}

	/***
	 * 将JSON字符串转换为java对象
	 * @param jsonStr jsonStr
	 * @param clazz clazz
	 * @return T
	 */
	public static <T> T toJavaObject(String jsonStr, Class<T> clazz) {
		try {
			return getObjectMapper().readValue(jsonStr, clazz);
		} catch (Exception e) {
			log.error("Json转Java异常", e);
			return null;
		}
	}

	/***
	 * 将JSON字符串转换为Map<String, Object></>对象
	 * @param jsonStr jsonStr
	 * @return Map
	 */
	public static Map<String, Object> parseObject(String jsonStr) {
		try {
			JavaType javaType = getObjectMapper().getTypeFactory().constructParametricType(Map.class, String.class, Object.class);
			return getObjectMapper().readValue(jsonStr, javaType);
		} catch (Exception e) {
			log.error("Json转Java异常", e);
			return null;
		}
	}

	/***
	 * 将JSON字符串转换为java对象
	 * @param jsonStr jsonStr
	 * @param clazz clazz
	 * @return T
	 */
	public static <T> T parseObject(String jsonStr, Class<T> clazz) {
		return toJavaObject(jsonStr, clazz);
	}

	/***
	 * 将JSON字符串转换为复杂类型的Java对象
	 * @param jsonStr jsonStr
	 * @param typeReference typeReference
	 * @return T
	 */
	public static <T> T parseObject(String jsonStr, TypeReference<T> typeReference) {
		try {
			return getObjectMapper().readValue(jsonStr, typeReference);
		} catch (Exception e) {
			log.error("Json转Java异常", e);
			return null;
		}
	}


	/***
	 * 将JSON字符串转换为list对象
	 * @param jsonStr jsonStr
	 * @return List
	 */
	public static <T> List<T> parseArray(String jsonStr, Class<T> clazz) {
		try {
			JavaType javaType = getObjectMapper().getTypeFactory().constructParametricType(List.class, clazz);
			return getObjectMapper().readValue(jsonStr, javaType);
		} catch (Exception e) {
			log.error("Json转Java异常", e);
			return null;
		}
	}

	/***
	 * 将JSON字符串转换为java对象
	 * @param jsonStr jsonStr
	 * @return Map
	 */
	public static Map toMap(String jsonStr) {
		return toJavaObject(jsonStr, Map.class);
	}

	/***
	 * 将JSON字符串转换为Map对象
	 * @param jsonStr jsonStr
	 * @return LinkedHashMap
	 */
	public static LinkedHashMap toLinkedHashMap(String jsonStr) {
		if (StringUtils.isEmpty(jsonStr)) {
			return null;
		}
		return toJavaObject(jsonStr, LinkedHashMap.class);
	}

	/**
	 * 判断String是否为Json
	 * @param str str
	 * @return 结果
	 */
	public static boolean isJSON(String str) {
		if(StringUtils.isBlank(str)){
			return false;
		}
		try {
			getObjectMapper().readValue(str, Object.class);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Map 转 pojo
	 * @param fromValue fromValue
	 * @param toValueType toValueType
	 * @return 结果
	 */
	public static <T> T toPojo(Map fromValue, Class<T> toValueType) {
		return getObjectMapper().convertValue(fromValue, toValueType);
	}

	/**
	 * resultNode 转 pojo
	 * @param resultNode resultNode
	 * @param toValueType toValueType
	 * @return 结果
	 */
	public static <T> T toPojo(JsonNode resultNode, Class<T> toValueType) {
		return getObjectMapper().convertValue(resultNode, toValueType);
	}

	/**
	 * 转 JsonNode
	 */
	public static JsonNode readTree(String jsonString) {
		try {
			return getObjectMapper().readTree(jsonString);
		} catch (IOException e) {
			log.error("Json读树异常", e);
			return null;
		}
	}

	public static JsonNode readTree(InputStream in) {
		try {
			return getObjectMapper().readTree(in);
		} catch (IOException e) {
			log.error("Json读树异常", e);
			return null;
		}
	}

	public static JsonNode readTree(byte[] content) {
		try {
			return getObjectMapper().readTree(content);
		} catch (IOException e) {
			log.error("Json读树异常", e);
			return null;
		}
	}

	public static JsonNode readTree(JsonParser jsonParser) {
		try {
			return getObjectMapper().readTree(jsonParser);
		} catch (IOException e) {
			log.error("Json读树异常", e);
			return null;
		}
	}


}