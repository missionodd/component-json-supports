package com.github.json;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ComponentJsonSupportsApplication {

    public static void main(String[] args) {
        SpringApplication.run(ComponentJsonSupportsApplication.class, args);
    }

}
