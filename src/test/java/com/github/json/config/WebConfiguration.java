/*
 * Copyright © 2019-2022  MIT
 */
package com.github.json.config;


import com.fasterxml.jackson.databind.ObjectMapper;

import com.github.json.utils.JsonUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.text.SimpleDateFormat;
import java.util.TimeZone;

/**
 * Diboot Core自动配置类
 *
 * @author mission
 * @version v2.0
 * @since 2019/08/01
 */
@Configuration
public class WebConfiguration implements WebMvcConfigurer {

    @Value("${spring.jackson.date-format:"+ DateConstants.yyyy_MM_dd_HH_mm_ss+"}")
    private String defaultDatePattern;

    @Value("${spring.jackson.time-zone:"+ DateConstants.TIME_ZONE+"}")
    private String defaultTimeZone;
    @Bean
    @ConditionalOnMissingBean
    public HttpMessageConverters jacksonHttpMessageConverters() {
        ObjectMapper objectMapper = JsonUtils.getObjectMapper();

        // 设置时区和日期转换
        objectMapper.setDateFormat(new SimpleDateFormat(defaultDatePattern));
        objectMapper.setTimeZone(TimeZone.getTimeZone(defaultTimeZone));

        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        // 设置格式化内容
        converter.setObjectMapper(objectMapper);
        return new HttpMessageConverters(converter);
    }

}