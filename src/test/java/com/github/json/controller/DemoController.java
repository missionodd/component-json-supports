/*
 * Copyright © 2019-2022  MIT
 */
package com.github.json.controller;


import com.github.json.entity.DemoUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Date;

import static java.time.LocalDateTime.now;

@Slf4j
@RestController
@RequestMapping("/test")
public class DemoController {



    @GetMapping("/")
    public ResponseEntity<DemoUser> get(DemoUser user) {
        log.info("{}", user);

        user = new DemoUser()
                .setId(1L)
                .setId(1L)
                .setGender("男")
                .setUsername("张三丰田")
                .setPassword("123456789")
                .setEmail("干扰干扰123456789@163.com干扰干扰")
                .setPhone("干扰17622233345干扰")
                .setIdCard("610124200001013315")
                .setBankCard("  6222020111782831934 ")
                .setAddress("上海市嘉定区南翔镇")
                .setBirthdate(new Date())
                .setFormatDate(new Date())
                .setEpochSecond(now())
                .setEpochMilli(now())
                .setDefaultZone(now())
                .setUtcZone(now())
                .setUtc8Zone(now())
                .setGmtZone(now())
                .setGmt8Zone(now())
                .setParisZone(now())
                .setLocalDateTime(now())
                .setLocalDate(LocalDate.now())
                .setEpochDay(LocalDate.now())
                .setFormatLocalDate(LocalDate.now());

        return ResponseEntity.ok(user);
    }

    @PostMapping("/")
    public ResponseEntity<DemoUser> post(@RequestBody DemoUser user) {
        log.info("{}", user);
        return ResponseEntity.ok(user);
    }


}
