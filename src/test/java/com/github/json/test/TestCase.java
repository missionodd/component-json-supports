package com.github.json.test;

import com.github.json.entity.DemoUser;
import com.github.json.utils.JsonUtils;
import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.TimeZone;

import static java.time.LocalDateTime.now;

public class TestCase {


    @Test
    public void testEpoch(){
        LocalDateTime now =LocalDateTime.now();
        long epochMilli = now.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
        long epochSecond = now.atZone(ZoneId.systemDefault()).toInstant().getEpochSecond();
        Assert.isTrue(epochMilli > epochSecond, epochMilli+","+epochSecond);
    }

    @Test
    public void testZone(){
        System.out.println(ZoneOffset.UTC.getId());
        System.out.println(ZoneId.of("UTC+8").getId());

        System.out.println(ZoneId.systemDefault());
        System.out.println(ZoneOffset.systemDefault());
        System.out.println(TimeZone.getDefault().getID());
        System.out.println(LocalDateTime.now());

        System.setProperty("user.timezone", "Europe/Paris");
        System.out.println(ZoneId.systemDefault());
        System.out.println(ZoneOffset.systemDefault());
        System.out.println(TimeZone.getDefault().getID());
        System.out.println(LocalDateTime.now());

        LocalDateTime time1 = LocalDateTime.now()
                .atZone(ZoneId.of("Europe/Paris"))
                .toLocalDateTime();
        System.out.println(time1);

        LocalDateTime time2 = LocalDateTime.now()
                .atZone(ZoneId.of("Europe/Paris"))
                .withZoneSameInstant(ZoneId.of("Europe/Paris"))
                .toLocalDateTime();
        System.out.println(time2);

        LocalDateTime time3 = LocalDateTime.now()
                .atZone(ZoneId.systemDefault())
                .withZoneSameLocal(ZoneId.of("Europe/Paris"))
                .toLocalDateTime();
        System.out.println(time3);

        LocalDateTime time4 = LocalDateTime.now()
                .atZone(ZoneId.systemDefault())
                .withZoneSameInstant(ZoneId.of("Europe/Paris"))
                .toLocalDateTime();
        System.out.println(time4);

    }


    @Test
    public void testJSON(){
        DemoUser  user = new DemoUser()
                .setId(1L)
                .setGender("男")
                .setUsername("张三丰田")
                .setPassword("123456789")
                .setEmail("干扰干扰123456789@163.com干扰干扰")
                .setPhone("干扰17622233345干扰")
                .setIdCard("610124200001013315")
                .setBankCard("  6222020111782831934 ")
                .setAddress("上海市嘉定区南翔镇")
                .setWebsite("www.baidu.com")
                .setHeadImage("headImage")
                .setDoubleSum(Double.valueOf("1234.35465"))
                .setMoney(BigDecimal.valueOf(Double.valueOf("1234.35465")))
                .setBirthdate(new Date())
                .setFormatDate(new Date())
                .setEpochSecond(now())
                .setEpochMilli(now())
                .setDefaultZone(now())
                .setUtcZone(now())
                .setUtc8Zone(now())
                .setGmtZone(now())
                .setGmt8Zone(now())
                .setParisZone(now())
                .setLocalDateTime(now())
                .setLocalDate(LocalDate.now())
                .setEpochDay(LocalDate.now())
                .setFormatLocalDate(LocalDate.now());

        String json = JsonUtils.toPrettyJSON(user);
        System.out.println(json);
    }
}

