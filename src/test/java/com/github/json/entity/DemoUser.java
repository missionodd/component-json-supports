
package com.github.json.entity;



import com.fasterxml.jackson.annotation.JsonFormat;
import com.github.json.annotations.*;
import com.github.json.desensitization.StringDesensitization;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;


@Data
@Accessors(chain = true)

public class DemoUser {


    private Long id;

    private Long departmentId;

    @JsonFormatter("http://%s")
    private String website;

    @JsonFormatter("%.2f")
    private Double doubleSum;


    @JsonFormatter("%.2f")
    private BigDecimal money;

    @JsonFormatter("https://%s_32x32")
    private String headImage;

    @PersonNameDesensitize
    private String username;


    private String gender;

    @StringDesensitize
    private String password;

    @PhoneDesensitize
    private String phone;

    @IDCardDesensitize
    private String idCard;


    @EmailDesensitize
    private String email;


    @AddressDesensitize
    private String address;


    @BankCardDesensitize
    private String bankCard;


    private Date birthdate;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date formatDate;

    @JsonEpochSecondTrans
    private LocalDateTime epochSecond;


    @JsonEpochMilliTrans
    private LocalDateTime epochMilli;


    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime defaultZone;

    @JsonFormat(timezone = "UTC", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime utcZone;

    @JsonFormat(timezone = "UTC+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime utc8Zone;


    @JsonFormat(timezone = "GMT", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime gmtZone;

    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime gmt8Zone;

    @JsonFormat(timezone = "Europe/Paris", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime parisZone;

    private LocalDateTime localDateTime;


    private LocalDate localDate;

    @JsonEpochDayTrans
    private LocalDate epochDay;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate formatLocalDate;


}
