# component-json-supports
Extended some support for Jackson

## Example 1
```
@Data
@Accessors(chain = true)
public class DemoUser {

    private Long id;

    @JsonFormatter("http://%s")
    private String website;

    @JsonFormatter("%.2f")
    private Double doubleSum;

    @JsonFormatter("%.2f")
    private BigDecimal money;

    @JsonFormatter("https://%s_32x32")
    private String headImage;

    @PersonNameDesensitize
    private String username;

    private String gender;

    @StringDesensitize
    private String password;

    @PhoneDesensitize
    private String phone;

    @IDCardDesensitize
    private String idCard;

    @EmailDesensitize
    private String email;

    @AddressDesensitize
    private String address;

    @BankCardDesensitize
    private String bankCard;

    private Date birthdate;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date formatDate;

    @JsonEpochSecondTrans
    private LocalDateTime epochSecond;

    @JsonEpochMilliTrans
    private LocalDateTime epochMilli;
    ...
}
```
```
 DemoUser  user = new DemoUser()
        .setId(1L)
        .setGender("男")
        .setUsername("张三丰田")
        .setPassword("123456789")
        .setEmail("干扰干扰123456789@163.com干扰干扰")
        .setPhone("干扰17622233345干扰")
        .setIdCard("610124200001013315")
        .setBankCard("  6222020111782831934 ")
        .setAddress("上海市嘉定区南翔镇")
        .setWebsite("www.baidu.com")
        .setHeadImage("headImage")
        .setDoubleSum(Double.valueOf("1234.35465"))
        .setMoney(BigDecimal.valueOf(Double.valueOf("1234.35465")))
        .setBirthdate(new Date())
        .setFormatDate(new Date())
        .setEpochSecond(now())
        .setEpochMilli(now())
        .setDefaultZone(now())
        .setUtcZone(now())
        .setUtc8Zone(now())
        .setGmtZone(now())
        .setGmt8Zone(now())
        .setParisZone(now())
        .setLocalDateTime(now())
        .setLocalDate(LocalDate.now())
        .setEpochDay(LocalDate.now())
        .setFormatLocalDate(LocalDate.now());

        String json = JsonUtils.toPrettyJSON(user);
        System.out.println(json);

```
Output
```
{
  "id" : 1,
  "website" : "http://www.baidu.com",
  "doubleSum" : "1234.35",
  "money" : "1234.35",
  "headImage" : "https://headImage_32x32",
  "username" : "张**田",
  "gender" : "男",
  "password" : "*********",
  "phone" : "干扰176****3345干扰",
  "idCard" : "6101**********3315",
  "email" : "干扰干扰*********@163.com干扰干扰",
  "address" : "上海市嘉定区***",
  "bankCard" : "***************1934",
  "birthdate" : "2023-02-03 11:07:48",
  "formatDate" : "2023-02-03 11:07:48",
  "epochSecond" : 1675393668,
  "epochMilli" : 1675393668833,
  "defaultZone" : "2023-02-03 11:07:48",
  "utcZone" : "2023-02-03 11:07:48",
  "utc8Zone" : "2023-02-03 11:07:48",
  "gmtZone" : "2023-02-03 11:07:48",
  "gmt8Zone" : "2023-02-03 11:07:48",
  "parisZone" : "2023-02-03 11:07:48",
  "localDateTime" : "2023-02-03 11:07:48",
  "localDate" : "2023-02-03",
  "epochDay" : 19391,
  "formatLocalDate" : "2023-02-03"
}
```
